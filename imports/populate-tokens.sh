#! /bin/bash
# Populate tokens from environment

ALERT_DIR="/etc/grafana/provisioning/alerting/"

if [[ -n "${SLACK_TOKEN}" ]]; then
  yq -i '(.contactPoints[].receivers[] | select(has("type")) | select(.type == "slack")) .settings.token = ("${SLACK_TOKEN}" | envsubst)' ${ALERT_DIR}/cp.yaml
else
  echo "SLACK_TOKEN not set alarm notifications for slack will not work!" >&2
fi
