#! /bin/bash

echo "Starting grafana, with provisioned alarms"
#/opt/grafana-import/import-rules.sh &  # disabled due to incompatibility with alert provisioning
/opt/grafana-import/populate-tokens.sh
/run.sh
