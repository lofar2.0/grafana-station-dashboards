FROM grafana/grafana

USER root

RUN apk --no-cache add curl
RUN apk --no-cache add jq

RUN wget https://github.com/mikefarah/yq/releases/download/v4.44.1/yq_linux_amd64.tar.gz -O - |\
  tar xz && mv yq_linux_amd64 /usr/bin/yq && chmod +x /usr/bin/yq

USER grafana

# Install some plugins
RUN grafana cli plugins install briangann-datatable-panel
RUN grafana cli plugins install ae3e-plotly-panel
RUN grafana cli plugins install yesoreyeram-infinity-datasource
RUN grafana cli plugins install aceiot-svg-panel
RUN grafana cli plugins install yesoreyeram-boomtable-panel
RUN grafana cli plugins install orchestracities-map-panel
RUN grafana cli plugins install larona-epict-panel
RUN grafana cli plugins install grafana-oncall-app
RUN grafana cli plugins install innius-grpc-datasource
RUN grafana cli plugins install nline-plotlyjs-panel

RUN wget https://algenty.github.io/flowcharting-repository/archives/agenty-flowcharting-panel-1.0.0b-SNAPSHOT.zip -O /tmp/agenty-flowcharting-panel.zip
RUN cd /var/lib/grafana/plugins/ && unzip /tmp/agenty-flowcharting-panel.zip && mv grafana-flowcharting agenty-flowcharting-panel

COPY grafana.ini /etc/grafana/grafana.ini
COPY imports/populate-tokens.sh /opt/grafana-import/populate-tokens.sh

# Add default configuration through provisioning (see https://grafana.com/docs/grafana/latest/administration/provisioning)
# https://grafana.com/docs/grafana/latest/alerting/set-up/provision-alerting-resources/file-provisioning/
COPY alerting /etc/grafana/provisioning/alerting/
COPY datasources /etc/grafana/provisioning/datasources/
COPY station-dashboards.yaml /etc/grafana/provisioning/dashboards/
COPY dashboards /var/lib/grafana/dashboards/station/station/
COPY panels /var/lib/grafana/panels/panels/

COPY run-wrapper.sh /run-wrapper.sh
ENTRYPOINT ["/run-wrapper.sh"]
