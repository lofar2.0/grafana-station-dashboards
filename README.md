# Grafana Dashboards for a LOFAR 2.0 station

These dashboards show the state of a single station. They are tailored to be installed:

* Locally on a station, through https://git.astron.nl/lofar2.0/tango/-/tree/master/docker/grafana
* Centrally to monitor a group of stations, through https://git.astron.nl/lofar2.0/operations-central-management/-/tree/main/grafana-central/dashboards

## Environment variables

This container uses the following environment variables upon starting for configuration

1. `SLACK_TOKEN`: Authentication token for slack alarms

## Datasources

The Grafana installation in this repo exposes and uses the following data sources,
as configured in the `dashboards/` directory:

* Prometheus, at `http://prometheus:9090`, serving metrics,
* Loki, at `http://loki:3100`, serving logs,
* Alerta, at `http://alerta-server:8080`, serving the Alerta HTTP API,
* Grafana, serving its own HTTP API.

## Development

To cover both these use cases, the designer must consider:

* Using a `host="$station"` filter to make sure that centrally, the selected station is shown. On a specific station, this will default to `host="localhost"`,
* Deploying changes on all stations as well as centrally.

## Deployment

To deploy changes, they must be:

1. Commit to this repository,
2. The submodules in the tango and operations-central-management repositories must link to the new commit,
3. Those repositories need to be redeployed on the stations and centrally, respectively.
